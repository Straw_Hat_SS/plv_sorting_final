import datetime

from airflow import DAG
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.hooks.base_hook import BaseHook
from airflow.operators.slack_operator import SlackAPIPostOperator
from airflow.contrib.operators.slack_webhook_operator import SlackWebhookOperator

SLACK_CONN_ID = 'slack-jt'
REDSHIFT_CONN_ID = 'redshift-jt'


def task_fail_slack_alert(context):
    slack_channel = BaseHook.get_connection(SLACK_CONN_ID).login
    slack_token = BaseHook.get_connection(SLACK_CONN_ID).password

    slack_msg = """
            :red_circle: Task Failed. 
            *Task*: {task}  
            *Dag*: {dag} 
            *Execution Time*: {exec_date}  
            *Log Url*: {log_url} 
            """.format(
        task=context.get('task_instance').task_id,
        dag=context.get('task_instance').dag_id,
        ti=context.get('task_instance'),
        exec_date=context.get('execution_date'),
        log_url=context.get('task_instance').log_url,
    )
    failed_alert = SlackAPIPostOperator(
        task_id='slack_test',
        channel=slack_channel,
        token=slack_token,
        text=slack_msg,
        username='Airflow')
    return failed_alert.execute(context=context)


def task_success_slack_alert(context):
    slack_channel = BaseHook.get_connection(SLACK_CONN_ID).login
    slack_token = BaseHook.get_connection(SLACK_CONN_ID).password

    slack_msg = """
            :large_green_circle: Task Successful. 
            *Task*: {task}  
            *Dag*: {dag} 
            *Execution Time*: {exec_date}  
            *Log Url*: {log_url} 
            """.format(
        task=context.get('task_instance').task_id,
        dag=context.get('task_instance').dag_id,
        ti=context.get('task_instance'),
        exec_date=context.get('execution_date'),
        log_url=context.get('task_instance').log_url,
    )
    failed_alert = SlackAPIPostOperator(
        task_id='slack_test',
        channel=slack_channel,
        token=slack_token,
        text=slack_msg,
        username='Airflow')
    return failed_alert.execute(context=context)


default_args = {
    'owner': 'airflow',
    'start_date': datetime.datetime(2021, 12, 30),
    'retries': 5,
    'retry_delay': datetime.timedelta(minutes=5),
    'on_failure_callback': task_fail_slack_alert,
}

# with DAG(
#     dag_id='Test',
#     default_args=default_args,
#     schedule_interval="0 */3 * * *",
# ) as dag:
#     query_execution = PostgresOperator(
#         task_id="redshift_query",
#         postgres_conn_id=REDSHIFT_CONN_ID,
#         sql=sql_query,
#     )

#     query_execution

def PLV_sorting():
    import pandas as pd
    import numpy as np
    import ast
    from fuzzywuzzy import fuzz
    from fuzzywuzzy import process
    import psycopg2
    import datetime
    import ast
    from sqlalchemy import create_engine
    import matplotlib.pyplot as plt
    engine = create_engine('postgresql+psycopg2://sai_sandeep:DaaLaVFs4QLc7@datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com:5439/datawarehousedb',echo=True)
    engine.connect()
    engine.clear_compiled_cache()


    # ## Function for connecting it to Database 

    # In[ ]:


    def getData(queryString_input):
        output = []
        redshiftdata = {}
        list1 = []
        try:
            con = psycopg2.connect(host='datawarehouse-cluster.cgatqp75ezrh.ap-southeast-1.redshift.amazonaws.com', user='sai_sandeep', password='DaaLaVFs4QLc7', database="datawarehousedb", port=5439)
            query = queryString_input
            cur = con.cursor()
            cur.execute(query)
            output = cur.fetchall()
            con.close()
            return output
        except psycopg2.Error as e:
            
            print("Error Occurred -> ", e)
            return []


    # # Query

    # ## City Clusters Data

    # In[ ]:


    cluster_city = '''
    select distinct
    addresscity
    , case
        when addresscity in (
        'Hubballi'
        , 'Belgaum'
        , 'Dharwad'
        , 'Davanagere'
        , 'Haveri'
        )
        then 'Hubballi'
        when addresscity in (
        'Bengaluru'
        , 'Mysore'
        , 'Tumkur'
        , 'Hosur'
        , 'Bangalore'
        , 'BENGALURU'
        , 'BANGALORE'
        , 'Mandya'
        , 'Chikkaballapura'
        , 'Hassan'
        )
        then 'Bangalore'
        when addresscity in (
        'Vijayawada'
        , 'Guntur'
        , 'GUNTUR'
        )
        then 'Vijayawada'
        when addresscity in (
        'Tiruppur'
        , 'Coimbatore'
        , 'Erode'
        , 'Mettupalayam'
        , 'Salem'
        , 'Pollachi'
        )
        then 'Coimbatore'
        when addresscity in (
        'Mohali'
        , 'Ziarakpur'
        , 'Zirakpur'
        , 'Mohali'
        , 'Amabla'
        , 'Panchkula'
        , 'Parwanoo'
        , 'Amabala'
        , 'Kharar'
        , 'Kurali'
        , 'Patiala'
        , 'Chandigarh'
        )
        then 'Chandigarh'
        when addresscity in (
        'Vizianagaram'
        , 'Vizag'
        )
        then 'Vizag'
        when addresscity in (
        'Gurgaon'
        , 'Faridabad'
        )
        then 'Gurgaon'
        when addresscity in (
        'Lucknow'
        , 'Kanpur'
        )
        then 'Lucknow'
        when addresscity in (
        'Jaipur'
        )
        then 'Jaipur'
        when addresscity in (
        'Ranchi'
        )
        then 'Ranchi'
        when addresscity in (
        'Pune'
        )
        then 'Pune'
        when addresscity in (
        'Hyderabad'
        )
        then 'Hyderabad'
        when addresscity in (
        'Trichy'
        )
        then 'Trichy'
        when addresscity in (
        'Chennai'
        )
        then 'Chennai'
        else 'ROI'
    end as clusters_
    from
    address_snapshot_
    '''
    df_cluster =pd.read_sql(cluster_city,engine)


    # ## Order data past 30days

    # In[ ]:


    orders_data_jpin = '''

            select
            date(ord.src_created_time)
            , addresscity
            , mskuid as jpin
            , count(distinct businessid) as buyers
            , sum(ord.order_item_amount) as gmv
            from
            bolt_order_item_snapshot_ ord
            join customer_snapshot_ c on
                c.customerid = ord.buyer_id
                and c.istestcustomer is false
            and c.businessid not in (
                'BZID-testPuja'
                , 'BZID-tech'
                , 'BZID-sajal'
                , 'BZID-merchCatalog'
                , 'BZID-1304457254'
            )
            join address_snapshot_ ads on ads.addressentityid = c.businessid
            and addresstype = 'SHIPPING'
            left join listing_snapshot ls on
            ls.listing_id = ord.listing_id
            left join sellerproduct_snapshot sps on
            sps.sp_id = ls.sp_id
            left join product_snapshot_ prod on
            prod.jpin = sps.jpin
            join msku on
            msku.jpin = prod.jpin
            where
            date(ord.src_created_time) >= current_date - 33
            and date(ord.src_created_time) < current_date - 3
            and ord.boltordertype = 'MARKETPLACE'
            group by
            1
            , 2
            ,3
    '''
    orders_data_jpin = getData(orders_data_jpin)


    # In[ ]:


    df_orders_data_jpin=pd.DataFrame(orders_data_jpin, columns = ['date','addresscity','jpin','buyers','jpin_gmv'])
    df_orders_data_jpin = df_orders_data_jpin.merge(df_cluster,how='left',on='addresscity')

    ### taking the mean
    df_orders_data_jpin = df_orders_data_jpin.groupby(['clusters_','jpin'],as_index=False).aggregate(np.mean)
    ls_gmv =[np.percentile(df_orders_data_jpin['jpin_gmv'],i) for i in [25,50,75] ]


    # ### Segmenting the cuts

    # In[ ]:


    df_final = pd.DataFrame()
    for i in df_orders_data_jpin.clusters_.unique():
        df_temp = df_orders_data_jpin[df_orders_data_jpin.clusters_==i].copy()
        ls_view =[np.percentile(df_temp['jpin_gmv'],j) for j in [25,50,75] ]
        condition_views = [
        (df_temp['jpin_gmv']>=ls_view[2])
        ,(df_temp['jpin_gmv']>=ls_view[1]) & (df_temp['jpin_gmv']<ls_view[2])
        ,(df_temp['jpin_gmv']>=ls_view[0]) & (df_temp['jpin_gmv']<ls_view[1])
        ,(df_temp['jpin_gmv']<ls_view[0])]
        val = ['Top_1','Top_2','Top_3','Top_4']
        df_temp['gmv_cuts']=np.select(condition_views,val)
        df_final=df_final.append(df_temp)
    df_gmv_cuts = df_final.copy()


    # ## Views data past 30days

    # In[ ]:


    qurey='''
    select
        date(ts),
        addresscity,
        mskuid as jpin,
        case
            when mskuid like '50%%' then mskuid
            else brandid
        end as brandid,
        prod.pvid,
        cat.distributed,
        count(distinct businessid) as total_views
    from
        product_oos_views pov
        join customer_snapshot_ c on c.customerid = pov.customerid
        and c.istestcustomer is false
        and c.businessid not in (
            'BZID-testPuja',
            'BZID-tech',
            'BZID-sajal',
            'BZID-merchCatalog',
            'BZID-1304457254'
        )
        join address_snapshot_ ads on ads.addressentityid = c.businessid
        and addresstype = 'SHIPPING'
        join msku on msku.jpin = pov.jpin
        left join product_snapshot_ prod on prod.jpin = pov.jpin
        left join category cat on cat.pvid = prod.pvid
    where
        date(ts) = {}
        and views != 'load'
    group by
        1,
        2,
        3,
        4,
        5,
        6
    '''


    # ### Iterating the Over 30days data

    # In[ ]:


    dt=[i.strftime('%Y-%m-%d') for i in pd.date_range(end=datetime.date.today()-datetime.timedelta(days=1),freq='D',periods=30)]


    # In[ ]:


    df = pd.DataFrame()
    for i in dt:
        df_temp =pd.read_sql(qurey.format('\''+i+'\''),engine)
        df = df.append(df_temp)


    # ### dtyping and adding city clusters

    # In[ ]:


    df['total_views']=df['total_views'].astype('int64')


    # In[ ]:


    df_views_data_jpin = df.merge(df_cluster,how='left',on='addresscity')
    df_views_data_jpin['total_views']=df_views_data_jpin['total_views'].astype('int64')
    df_views_data_jpin = df_views_data_jpin[['clusters_','jpin','total_views']].groupby(['clusters_','jpin'],as_index=False).aggregate(np.mean)


    # ### Segmenting the cuts

    # In[ ]:


    df_final = pd.DataFrame()
    for i in df_views_data_jpin.clusters_.unique():
        df_temp = df_views_data_jpin[df_views_data_jpin.clusters_==i].copy()
        ls_view =[np.percentile(df_temp['total_views'],i) for i in [25,50,75] ]
        condition_views = [
        (df_temp['total_views']>=ls_view[2])
        ,(df_temp['total_views']>=ls_view[1]) & (df_temp['total_views']<ls_view[2])
        ,(df_temp['total_views']>=ls_view[0]) & (df_temp['total_views']<ls_view[1])
        ,(df_temp['total_views']<ls_view[0])]
        val = ['Top_1','Top_2','Top_3','Top_4']
        df_temp['views_cut']=np.select(condition_views,val)
        df_final=df_final.append(df_temp)
    df_views_cuts = df_final.copy()


    # ## 1. Distinct_jpins

    # In[ ]:


    distinct_jpins ='''
    select
    distinct addresscity,
        mskuid
    --    prod.pvid,
    --    case
    --        when mskuid like '50%%' then mskuid
    --        else brandid
    --    end as brandid,
    --    cat.distributed
    from
        product_oos_views pov
        join msku on msku.jpin = pov.jpin
    --    left join product_snapshot_ prod on prod.jpin = pov.jpin
    --    left join category cat on cat.pvid = prod.pvid
        left join customer_snapshot_ cs on cs.customerid = pov.customerid
        join address_snapshot_ ads on ads.addressentityid = cs.businessid
        and addresstype = 'SHIPPING'
        and cs.businessid not in (
            'BZID-testPuja',
            'BZID-tech',
            'BZID-sajal',
            'BZID-merchCatalog',
            'BZID-1304457254',
            'BZID-1304463418',
            'BZID-hubballi',
            'BZID-1304433825',
            'BZID-1304436504',
            'BZID-1304435850'
        )
    where
        date(ts) >= current_date - 30
        and date(ts) < current_date
    '''

    df_distinct_jpins = getData(distinct_jpins)


    # In[ ]:


    df_distinct_jpins=pd.DataFrame(df_distinct_jpins, columns = ['addresscity','jpin'])


    # ## 2. Views_data_jpin

    # In[ ]:


    views_data_jpin = '''

    select
        date(ts),
        addresscity,
        mskuid as jpin,
        case
            when mskuid like '50%' then mskuid
            else brandid
        end as brandid,
        prod.pvid,
        cat.distributed,
        count(distinct businessid) as total_views
    from
        product_oos_views pov
        join customer_snapshot_ c on c.customerid = pov.customerid
        and c.istestcustomer is false
        and c.businessid not in (
            'BZID-testPuja',
            'BZID-tech',
            'BZID-sajal',
            'BZID-merchCatalog',
            'BZID-1304457254'
        )
        join address_snapshot_ ads on ads.addressentityid = c.businessid
        and addresstype = 'SHIPPING'
        join msku on msku.jpin = pov.jpin
        left join product_snapshot_ prod on prod.jpin = pov.jpin
        left join category cat on cat.pvid = prod.pvid
    where
        date(ts) >= current_date - 8
        and date(ts) < current_date
        and views != 'load'
    group by
        1,
        2,
        3,
        4,
        5,
        6

        
    '''
    df_views_data_jpin = getData(views_data_jpin)


    # In[ ]:


    df_views_data_jpin=pd.DataFrame(df_views_data_jpin, columns = ['date','addresscity','jpin','brandid','pvid','distributed','total_views'])


    # ## 3. brand_sales

    # In[ ]:


    brand_sales= '''

    select
        date(ord.src_created_time),
        addresscity,
        case
            when mskuid like '50%' then mskuid
            else prod.brandid
        end as brandid,
        count(distinct businessid) as brand_dau,
        sum(ord.order_item_amount) as gmv
    from
        bolt_order_item_snapshot_ ord
        join customer_snapshot_ c on c.customerid = ord.buyer_id
        and c.istestcustomer is false
        and c.businessid not in (
            'BZID-testPuja',
            'BZID-tech',
            'BZID-sajal',
            'BZID-merchCatalog',
            'BZID-1304457254'
        )
        join address_snapshot_ ads on ads.addressentityid = c.businessid
        and addresstype = 'SHIPPING'
        left join listing_snapshot ls on ls.listing_id = ord.listing_id
        left join sellerproduct_snapshot sps on sps.sp_id = ls.sp_id
        left join product_snapshot_ prod on prod.jpin = sps.jpin
        join msku on msku.jpin = prod.jpin
    where
        date(ord.src_created_time) >= current_date - 33
        and date(ord.src_created_time) < current_date - 3
        and ord.boltordertype = 'MARKETPLACE'
    group by
        1,
        2,
        3
        
    '''
    df_brand_sales = getData(brand_sales)


    # In[ ]:


    df_brand_sales=pd.DataFrame(df_brand_sales, columns = ['date','addresscity','brandid','brand_dau','brand_gmv'])


    # ## 4. pv_sales

    # In[ ]:


    pv_sales='''

    select
        date(ord.src_created_time),
        addresscity,
        prod.pvid,
        pv.pvname,
        count(distinct businessid) as pv_dau,
        sum(ord.order_item_amount) as gmv
    from
        bolt_order_item_snapshot_ ord
        join customer_snapshot_ c on c.customerid = ord.buyer_id
        and c.istestcustomer is false
        and c.businessid not in (
            'BZID-testPuja',
            'BZID-tech',
            'BZID-sajal',
            'BZID-merchCatalog',
            'BZID-1304457254'
        )
        join address_snapshot_ ads on ads.addressentityid = c.businessid
        and addresstype = 'SHIPPING'
        left join listing_snapshot ls on ls.listing_id = ord.listing_id
        left join sellerproduct_snapshot sps on sps.sp_id = ls.sp_id
        left join product_snapshot_ prod on prod.jpin = sps.jpin
        left join (
            select
                distinct pvid,
                pvname
            from
                productvertical_snapshot_
        ) pv on pv.pvid = prod.pvid
        join msku on msku.jpin = prod.jpin
    where
        date(ord.src_created_time) >= current_date - 33
        and date(ord.src_created_time) < current_date - 3
        and ord.boltordertype = 'MARKETPLACE'
    group by
        1,
        2,
        3,
        4
    '''
    df_pv_sales = getData(pv_sales)


    # In[ ]:


    df_pv_sales=pd.DataFrame(df_pv_sales, columns = ['date','addresscity','pvid','pvname','pv_dau','pv_gmv'])


    # ## 5. daily_distinct

    # In[ ]:


    daily_distinct = '''

    select
        date(ord.src_created_time),
        addresscity,
        count(distinct businessid) as dau,
        sum(ord.order_item_amount) as gmv
    from
        bolt_order_item_snapshot_ ord
        join customer_snapshot_ c on c.customerid = ord.buyer_id
        and c.istestcustomer is false
        and c.businessid not in (
            'BZID-testPuja',
            'BZID-tech',
            'BZID-sajal',
            'BZID-merchCatalog',
            'BZID-1304457254'
        )
        join address_snapshot_ ads on ads.addressentityid = c.businessid
        and addresstype = 'SHIPPING'
        left join listing_snapshot ls on ls.listing_id = ord.listing_id
        left join sellerproduct_snapshot sps on sps.sp_id = ls.sp_id
        left join product_snapshot_ prod on prod.jpin = sps.jpin
    where
        date(ord.src_created_time) >= current_date - 33
        and date(ord.src_created_time) < current_date - 3
        and ord.boltordertype = 'MARKETPLACE'
    group by
        1,
        2
        
    '''
    df_daily_distinct = getData(daily_distinct)


    # In[ ]:


    df_daily_distinct=pd.DataFrame(df_daily_distinct, columns = ['date','addresscity','dd_dau', 'dd_gmv'])


    # ## 6. orders_data_jpin

    # In[ ]:


    orders_data_jpin = '''

    select
        date(ord.src_created_time),
        addresscity,
        mskuid as jpin,
        count(distinct businessid) as buyers,
        sum(ord.order_item_amount) as gmv
    from
        bolt_order_item_snapshot_ ord
        join customer_snapshot_ c on c.customerid = ord.buyer_id
        and c.istestcustomer is false
        and c.businessid not in (
            'BZID-testPuja',
            'BZID-tech',
            'BZID-sajal',
            'BZID-merchCatalog',
            'BZID-1304457254'
        )
        join address_snapshot_ ads on ads.addressentityid = c.businessid
        and addresstype = 'SHIPPING'
        left join listing_snapshot ls on ls.listing_id = ord.listing_id
        left join sellerproduct_snapshot sps on sps.sp_id = ls.sp_id
        left join product_snapshot_ prod on prod.jpin = sps.jpin
        join msku on msku.jpin = prod.jpin
    where
        date(ord.src_created_time) >= current_date - 33
        and date(ord.src_created_time) < current_date - 3
        and ord.boltordertype = 'MARKETPLACE'
    group by
        1,
        2,
        3
        
    '''
    df_orders_data_jpin = getData(orders_data_jpin)


    # In[ ]:


    df_orders_data_jpin=pd.DataFrame(df_orders_data_jpin, columns = ['date','addresscity','jpin','buyers','jpin_gmv'])


    # ## 7. jpin_level_margin

    # In[ ]:


    jpin_level_margin = '''
    
    select
        order_date,
        addresscity,
        case
            when mskuid like '50%' then mskuid
            else p.jpin
        end as jpin,
        case
            when mskuid like '50%' then mskuid
            else p.title
        end as product_title,
        cat.distributed as FMCG_flag,
        cat.category_name,
        case
            when mskuid like '50%' then mskuid
            else b.displaytitle
        end as Brand --------------------------------------------------------------
    ,
        case
            when mskuid like '50%' then mskuid
            else b.internalname
        end as internalname ---------------------------------------------------------------
    ,
        case
            when mskuid like '50%' then mskuid
            else m.manufacturername
        end as manufacturername,
        pv.pvname as PV_name --- PLease Look HERE for net GMV
    ,
        sum(net_order_quantity * price_per_unit) as net_gmv,
        sum(
            shipping_per_unit * net_order_quantity + total_jw_margin_without_backend + total_backend_margin_jw + case
                when fulfilling_entity = 'SS' then 0
                else total_commission
            end + case
                when fulfilling_entity = 'SS' then total_backend_margin_ss + total_ss_margin_without_backend -0.001 * price_per_unit * net_order_quantity
                else 0
            end
        ) as margin,
        case
            when sum(net_order_quantity * price_per_unit) = 0 then 0
            else sum(
                shipping_per_unit * net_order_quantity + total_jw_margin_without_backend + total_backend_margin_jw + case
                    when fulfilling_entity = 'SS' then 0
                    else total_commission
                end + case
                    when fulfilling_entity = 'SS' then total_backend_margin_ss + total_ss_margin_without_backend -0.001 * price_per_unit * net_order_quantity
                    else 0
                end
            ) * 1.00 / sum(net_order_quantity * price_per_unit)
        end as margin_pct
    from
        daily_margin_snapshot mar
        left join bolt_order_item_snapshot_ ord on mar.order_item_id = ord.order_item_id
        join customer_snapshot_ c on c.customerid = ord.buyer_id
        join address_snapshot_ ads on ads.addressentityid = c.businessid
        and addresstype = 'SHIPPING'
        left join business_snapshot s on c.businessid = s.businessid
        left join listing_snapshot l on l.listing_id = ord.listing_id
        left join org_profile_snapshot org on org.org_profile_id = l.owner_id
        left join sellerproduct_snapshot sp on sp.sp_id = l.sp_id
        left join product_snapshot_ p on p.jpin = sp.jpin
        left join category cat on p.pvid = cat.pvid
        left join brand_snapshot_ b on b.brandid = p.brandid
        left join manufacturer_snapshot m on m.manufacturerid = b.manufacturerid
        join msku on msku.jpin = p.jpin
        left join (
            select
                distinct (pvid),
                pvname
            from
                productvertical_snapshot_
        ) pv on pv.pvid = p.pvid
    where
        c.istestcustomer is false
        and (
            c.status = 'ACTIVE'
            or c.status = 'ONHOLD'
        )
        and ord.order_item_amount > 0
        and order_date >= current_date - 8
        and order_date <= current_date - 1
    group by
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10
        
    '''

    df_jpin_level_margin = getData(jpin_level_margin)


    # In[ ]:


    df_jpin_level_margin = pd.DataFrame(df_jpin_level_margin, columns = ['order_date','addresscity','jpin','product_title','FMCG_flag','category_name','Brand','internalname','manufacturername','PV_name','net_gmv','margin','margin_pct'])


    # ## 8. returns_amt

    # In[ ]:


    returns_amt = '''

    select
        date(pem.created_time),
        addresscity,
        mskuid as jpin,
        sum(isnull(di.amount_difference, 0)) as returned_amount
    from
        payment_entity_mapping pem
        join bolt_order_item_v2_snapshot ord on pem.entity_id = ord.order_item_id
        join delta_item di on pem.mapping_id = di.mapping_id
        left join listing_snapshot ls on ls.listing_id = ord.listing_id
        left join sellerproduct_snapshot sps on sps.sp_id = ls.sp_id
        left join product_snapshot_ prod on prod.jpin = sps.jpin
        left join category cat on cat.pvid = prod.pvid
        join msku on msku.jpin = prod.jpin
        left join customer_snapshot_ cs on cs.customerid = ord.buyer_id
        join address_snapshot_ ads on ads.addressentityid = cs.businessid
    where
        trunc(pem.created_time) >= current_date - 8
        and trunc(pem.created_time) <= current_date - 1
        and pem.deleted_at is null
        and pem.is_deleted is null
        and di.deleted_at is null
        and entity_type = 'RETURN_TO_ORIGIN' --and extract('hour' from pem.created_time)  <= extract('hour' from current_timestamp)
    group by
        1,
        2,
        3
        
    '''
    df_returns_amt = getData(returns_amt)


    # In[ ]:


    df_returns_amt = pd.DataFrame(df_returns_amt,columns=['date','addresscity','jpin','returned_amount'])


    # ## 9. delivered_amt

    # In[ ]:


    delivered_amt = '''

    select
        date(pem.created_time),
        addresscity,
        mskuid as jpin,
        --di.reason as reason_code,
        sum(actual_amount) as delivered_amount
    from
        payment_entity_mapping pem
        join bolt_order_item_v2_snapshot ord on pem.entity_id = ord.order_item_id --join delta_item di on pem.mapping_id=di.mapping_id 
        left join listing_snapshot ls on ls.listing_id = ord.listing_id
        left join sellerproduct_snapshot sps on sps.sp_id = ls.sp_id
        left join product_snapshot_ prod on prod.jpin = sps.jpin
        left join category cat on cat.pvid = prod.pvid
        join msku on msku.jpin = prod.jpin
        left join customer_snapshot_ cs on cs.customerid = ord.buyer_id
        join address_snapshot_ ads on ads.addressentityid = cs.businessid
    where
        trunc(pem.created_time) >= current_date - 8
        and trunc(pem.created_time) <= current_date - 1
        and pem.deleted_at is null
        and pem.is_deleted is null --and di.deleted_at is null
        and entity_type = 'DELIVERED' --and extract('hour' from pem.created_time)  <= extract('hour' from current_timestamp)
    group by
        1,
        2,
        3
        
    '''

    df_delivered_amt = getData(delivered_amt)


    # In[ ]:


    df_delivered_amt = pd.DataFrame(df_delivered_amt,columns=['date','addresscity','jpin','delivered_amount'])


    # ## 10. names

    # In[ ]:


    names = '''
                -- jpin
            -- , brandid
            -- , pvid
            -- , distributed
            -- , product_title
            -- , category_name
            -- , Brand
            -- , manufacturername
            -- , PV_name
            select
            distinct case
                when mskuid like '50%'
                then mskuid
                else p.jpin
            end as jpin
            , case
                when mskuid like '50%'
                then mskuid
                else p.brandid
            end as brandid
            , p.pvid
            , cat.distributed
            , case
                when mskuid like '50%'
                then mskuid
                else p.title
            end as product_title
            , cat.category_name
            , case
                when mskuid like '50%'
                then mskuid
                else b.displaytitle
            end as Brand --------------------------------------------------------------
            , case
                when mskuid like '50%'
                then mskuid
                else b.internalname
            end as internalname ---------------------------------------------------------------
            , case
                when mskuid like '50%'
                then mskuid
                else m.manufacturername
            end as manufacturername
            , pv.pvname as PV_name
            from
            product_snapshot_ p
            left join category cat on
                cat.pvid = p.pvid
            left join brand_snapshot_ b on
                b.brandid = p.brandid
            left join manufacturer_snapshot m on
                m.manufacturerid = b.manufacturerid
            join msku on
                msku.jpin = p.jpin
            left join (
                select distinct
                (
                    pvid
                )
                , pvname
            from
                productvertical_snapshot_
            )
            pv on
                pv.pvid = p.pvid
            group by
            1
            , 2
            , 3
            , 4
            , 5
            , 6
            , 7
            , 8
            , 9
            , 10
    '''
    df_names = getData(names)


    # In[ ]:


    df_names = pd.DataFrame(df_names,columns=['jpin','brandid','pvid','distributed','product_title','category_name','Brand','internalname','manufacturername','PV_name'])


    # ## 11. cat_scores

    # In[ ]:


    category_score_type_mapping = '''
    SELECT * FROM category_score_type_mapping
    '''
    df_cat_scores = getData(category_score_type_mapping)


    # In[ ]:


    df_cat_scores = pd.DataFrame(df_cat_scores,columns=['category_name','type'])


    # ## 12. group Table

    # In[ ]:


    ## YET to be added


    # # Joint Family

    # In[ ]:


    df_jpin_level_conversion = df_distinct_jpins.merge(df_names,how='left',on='jpin')
    df_jpin_level_conversion = df_jpin_level_conversion.merge(df_views_data_jpin,on=['addresscity','jpin','brandid','pvid','distributed'],how='left')
    df_jpin_level_conversion = df_jpin_level_conversion.merge(df_orders_data_jpin,on=['date','addresscity','jpin'],how='left') 
    df_jpin_level_conversion = df_jpin_level_conversion.merge(df_daily_distinct,how='left',on=['date','addresscity'])
    df_jpin_level_conversion=df_jpin_level_conversion.merge(df_brand_sales,how ='left',on=['date','addresscity','brandid'])
    df_jpin_level_conversion=df_jpin_level_conversion.merge(df_pv_sales,how ='left',on=['date','addresscity','pvid'])


    # # Analysis

    # ## Jpin Level Margin is being mapped to clusters

    # In[ ]:


    df_jpin_level_margin=df_jpin_level_margin.merge(df_clusters,on='addresscity',how='left')
    df_jpin_level_margin.drop('addresscity',axis=1,inplace=True)
    df_jpin_level_margin.fillna('0',inplace=True)
    df_jpin_level_margin=df_jpin_level_margin[['order_date','clusters_','jpin','net_gmv','margin','margin_pct']]
    df_jpin_level_margin[['net_gmv','margin','margin_pct']]=df_jpin_level_margin[['net_gmv','margin','margin_pct']].astype('float64')
    df_jpin_level_margin=df_jpin_level_margin.groupby(['order_date','clusters_','jpin'],as_index=False).sum()


    # ## Return Amt is being mapped to clusters

    # In[ ]:


    df_returns_amt=df_returns_amt.merge(df_clusters,on='addresscity',how='left')
    df_returns_amt.drop('addresscity',axis=1,inplace=True)
    df_returns_amt.fillna('0',inplace=True)
    df_returns_amt['returned_amount']=df_returns_amt['returned_amount'].astype('float64')
    df_returns_amt=df_returns_amt.groupby(['date','clusters_','jpin'],as_index=False).sum()


    # ## Delivered Amt is being mapped to clusters

    # In[ ]:


    df_delivered_amt=df_delivered_amt.merge(df_clusters,on='addresscity',how='left')
    df_delivered_amt.drop('addresscity',axis=1,inplace=True)
    df_delivered_amt.fillna('0',inplace=True)
    df_delivered_amt['delivered_amount']=df_delivered_amt['delivered_amount'].astype('float64')
    df_delivered_amt=df_delivered_amt.groupby(['date','clusters_','jpin'],as_index=False).sum()


    # ### Jpin level conversion Table preparation

    # #### Fill the blanks with 0 values only for this columns 
    #  'total_views','buyers', 'jpin_gmv', 'dd_dau', 'dd_gmv', 'brand_dau','brand_gmv','pvname', 'pv_dau', 'pv_gmv'

    # In[ ]:


    df_jpin_level_conversion[['total_views',
        'buyers', 'jpin_gmv', 'dd_dau', 'dd_gmv', 'brand_dau', 'brand_gmv',
        'pvname', 'pv_dau', 'pv_gmv']] = df_jpin_level_conversion[['total_views',
        'buyers', 'jpin_gmv', 'dd_dau', 'dd_gmv', 'brand_dau', 'brand_gmv',
        'pvname', 'pv_dau', 'pv_gmv']].fillna(0)


    # #### Subseting apart of data creating df_cov

    # In[ ]:


    df_cov = df_jpin_level_conversion[['addresscity','jpin', 'pvid', 'brandid', 'distributed', 'date','total_views', 'buyers','dd_dau', 'jpin_gmv','brand_gmv','pv_gmv']].copy()
    df_cov=df_cov.merge(df_clusters,how ='left',on='addresscity')
    df_cov=df_cov[['date','clusters_','jpin','pvid','brandid', 'distributed','total_views', 'buyers', 'dd_dau', 'jpin_gmv', 'brand_gmv', 'pv_gmv']].groupby(['date','clusters_','jpin','pvid','brandid', 'distributed'],as_index=False).sum()


    # ### Calculation for Jpin Conversion which will give us the JPIN Score in a couple of code down the line

    # In[ ]:


    df_cov['conversion']=df_cov['buyers']/df_cov['total_views']
    df_cov['penetration']= df_cov['buyers']/df_cov['dd_dau']
    df_cov['gmv_share_wt_brand']=df_cov['jpin_gmv']/df_cov['brand_gmv']
    df_cov['gmv_share_wt_pv']= df_cov['jpin_gmv']/df_cov['pv_gmv']


    df_cov[['conversion','penetration', 'gmv_share_wt_brand', 'gmv_share_wt_pv']]=df_cov[['conversion','penetration', 'gmv_share_wt_brand', 'gmv_share_wt_pv']].fillna(0)


    # # To download Conversion related data you can uncomment the below Command 
    # ### df_cov.to_excel('df_con_25th_Dec.xlsx')

    # # Jpin Level Margin, return amount,  delivered amount

    # In[ ]:


    # Creating a df_final dataframe for GMV_share and realted stuff
    ## df_con <-- df_jpin_level_margin
    df_final = df_cov.merge(df_jpin_level_margin,left_on=['date','clusters_','jpin'],right_on=['order_date','clusters_','jpin'],how ='left')


    ## df_final <--- df_returns_amt
    df_final = df_final.merge(df_returns_amt,how='left',on=['date','clusters_','jpin'])


    ##df_final <--- df_delivered_amt
    df_final = df_final.merge(df_delivered_amt,how='left',on=['date','clusters_','jpin'])


    ## fillna null values with 0 in df_final
    df_final[['delivered_amount','returned_amount']]=df_final[['delivered_amount','returned_amount']].fillna(0)


    # # Return_PCT_GMV

    # In[ ]:


    # Typecasting
    df_final.returned_amount = df_final.returned_amount.astype('float64')
    df_final.delivered_amount = df_final.delivered_amount.astype('float64')


    # Returned_Amount/Delivered_Amount
    df_final['returns_pct_gmv']=df_final.apply(lambda x: x['returned_amount']/x['delivered_amount'] if x['delivered_amount']>0 else 0,axis=1)


    # # Final aggregation

    # ## Spliting df_final into different groups for calcualtions

    # In[ ]:


    df_final_grp_1 = df_final[['clusters_','jpin','pvid','brandid','distributed','conversion','penetration','returns_pct_gmv','margin_pct']].fillna(0).copy()
    df_final_grp_2= df_final[['clusters_','jpin','pvid','brandid','distributed','gmv_share_wt_brand','gmv_share_wt_pv','margin_pct','returns_pct_gmv']].copy()
    df_final_grp_3= df_final[['clusters_','jpin','pvid','brandid','distributed','date']].copy()


    # ## Calucltion for avg_cnv, avg_pen, avg_ret, avg_margin, 

    # In[ ]:


    df_final_grp_1 = df_final_grp_1.groupby(['clusters_','jpin','pvid','brandid','distributed'],as_index=False).agg(np.mean)
    df_final_grp_2 = df_final_grp_2.groupby(['clusters_','jpin','pvid','brandid','distributed'],as_index=False).agg(np.sum)
    df_final_grp_3 = df_final_grp_3.groupby(['clusters_','jpin','pvid','brandid','distributed'],as_index=False).count()


    # In[ ]:


    df_final_grp_2 = df_final_grp_2.merge(df_final_grp_3,on=['clusters_','jpin','pvid','brandid','distributed'],how = "left")
    df_final_grp_1['avg_gmv_share']=df_final_grp_2.apply(lambda x: x['gmv_share_wt_brand']/x['date'] if x['date']>0 else 0,axis=1)
    df_final_grp_1['avg_gmv_share_pv']=df_final_grp_2.apply(lambda x: x['gmv_share_wt_pv']/x['date'] if x['date']>0 else 0,axis=1)


    # In[ ]:


    df = df_final_grp_1.merge(df_names,on=['jpin','pvid','brandid','distributed'],how ="left")


    # ## Jpin Score

    # In[ ]:


    df['jpin_score']=0.4*df['penetration']+0.3*df['conversion']+0.2*df['margin_pct']-0.1*df['returns_pct_gmv']


    # In[ ]:


    df =df.merge(df_cat_scores,how = "left",on = 'category_name')


    # # Adding brand level score

    # In[ ]:


    brand_level_scores=pd.DataFrame()
    pv_level_scores=pd.DataFrame()
    brand_level_scores[['clusters_','brandid','internalname','Brand_score']]=df[['clusters_','brandid','internalname','avg_gmv_share','jpin_score']].groupby(['clusters_','brandid','internalname'],as_index=False).apply(lambda x: (x['avg_gmv_share']*x['jpin_score']).sum())
    pv_level_scores[['clusters_','pvid','pv_score']] = df[['clusters_','pvid','avg_gmv_share_pv','jpin_score']].groupby(['clusters_','pvid'],as_index=False).apply(lambda x: (x['avg_gmv_share_pv']*x['jpin_score']).sum())


    # In[ ]:


    df = df.merge(brand_level_scores,how ="left",on=['clusters_','brandid','internalname'])
    df = df.merge(pv_level_scores,how ="left",on=['clusters_','pvid'])


    # In[ ]:


    df['final_score']=df.apply(lambda x: x['Brand_score'] if x['type']!= 'PV' else x['pv_score'],axis=1)


    # # Newer version_using views PLV sorting Logic

    # In[ ]:


    df_views_cuts = pd.read_csv('/Users/Sai_Sandeep/PLV_Sorting_Folder/Plv_sorting_breakdown_geo/csv_data/views_data_30_days.csv',index_col=0)
    df_gmv_cuts = pd.read_csv('/Users/Sai_Sandeep/PLV_Sorting_Folder/Plv_sorting_breakdown_geo/csv_data/orders_data_30_days.csv',index_col=0)


    # ## Merging the view and GMV for Bukecting in Top_1 to Top_4

    # In[ ]:


    df= df.merge(df_views_cuts[['clusters_','jpin','views_cut','total_views']],on=['clusters_','jpin'],how='left')


    # df= df.merge(df_gmv_cuts[['clusters_','jpin','gmv_cuts','jpin_gmv']],on=['clusters_','jpin'],how='left')


    # ## Current ranking calculations

    # In[ ]:


    # df_done=pd.DataFrame()
    # for i in df.clusters_.unique():
    #     df_temp=df[df.clusters_==i]
    #     df_temp= df_temp.sort_values(['type','final_score','jpin_score'],ascending=False)
    #     df_temp['Geowise_system_rank']=[j for j in range(1,len(df_temp)+1)]
    #     df_done = df_done.append(df_temp)
    # df = df_done.copy()


    # ## Ranking the Jpin bases on Views and GMV (comented)

    # In[ ]:


    df_done=pd.DataFrame()
    for i in df.clusters_.unique():
        df_temp=df[df.clusters_==i].copy()
        df_temp.sort_values(['views_cut','jpin_score'],ascending=[True,False],inplace=True)
        df_temp['rank_views']=[j for j in range(1,len(df_temp)+1)]

        ### Uncomment below if you want GMV wise rank

        # df_temp.sort_values(['gmv_cuts','jpin_score'],ascending=[True,False],inplace=True)
        # df_temp['rank_gmv']=[j for j in range(1,len(df_temp)+1)]    
        df_done = df_done.append(df_temp)


    df=df_done.copy()
    df.to_sql('your_table_PLV_Sorting_Logic', engine, index=False, if_exists='replace')


dag = DAG(
  dag_id='Test', 
  description='Testing for PLV Logic',
  default_args=default_args)

src1_s3 = PythonOperator(
  task_id='PLV_Sorting_table', 
  python_callable=PLV_sorting, 
  dag=dag)
